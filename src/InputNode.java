/**
 * Created by tfy12jsg on 2015-10-05.
 */
public class InputNode {

    private Double value;

    public InputNode(Double value) {
        this.value = value;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value=value;
    }

}
