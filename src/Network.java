import java.util.ArrayList;

/**
 * Created by c12arr on 2015-10-09.
 */
public class Network {

    private ArrayList<OutputNode> outputNodes;
    private ArrayList<InputNode> inputNodes;

    public Network(int nrOfInputs, int nrOfOutputs) {
        outputNodes = new ArrayList<OutputNode>(nrOfOutputs);
        inputNodes = new ArrayList<InputNode>(nrOfInputs);


        for(int j = 0 ; j < nrOfInputs ; j++) {
            inputNodes.add(new InputNode(0.0));
        }

        for(int i = 0 ; i < nrOfOutputs ; i++) {
            ArrayList<Link> linkList = new ArrayList<Link>();

            for(int j = 0 ; j < nrOfInputs ; j++) {
                linkList.add(new Link(j, 0));
            }

            outputNodes.add(new OutputNode(linkList));
        }

    }

    public void train(Image img, double alpha) {

        setInputNodes(img);

        for(OutputNode outNode : outputNodes) {
            double sum = 0;
            for(Link link : outNode.getLinks()) {
                sum += link.getWeight() * inputNodes.get(link.getInputNode()).getValue();
            }
            outNode.setValue(Network.activationFunction(sum));

             int index = outputNodes.indexOf(outNode);

            double y = 0;

            if(img.getFacit() == State.parseState(index+1)) {
                y =1;
            }

            double error = y - outNode.getValue();
            for(Link link : outNode.getLinks()) {
                double deltaWeight = alpha*error*inputNodes.get(link.getInputNode()).getValue();
                link.setWeight(link.getWeight()+deltaWeight);
            }

        }

    }

    private void setInputNodes(Image img) {
        Double image[][] = img.getImg();
        int size = image[0].length;
        for(int i = 0 ; i < size ; i++) {
            for(int j = 0 ; j < size ; j++) {

            InputNode inNode = inputNodes.get((i*size)+j);
            inNode.setValue(image[i][j]);
            }
        }
    }

    public State test(Image img) {
        setInputNodes(img);

        for(OutputNode outNode : outputNodes) {
            double sum = 0;
            for (Link link : outNode.getLinks()) {
                sum += link.getWeight() * inputNodes.get(link.getInputNode()).getValue();
            }
            outNode.setValue(Network.activationFunction(sum));
        }
        double maxValue = 0;
        int max = 0;
        for(OutputNode outNode : outputNodes) {
            if(outNode.getValue() > maxValue) {
                max = outputNodes.indexOf(outNode);
                maxValue = outNode.getValue();
            }
        }

        return State.parseState(max+1);
    }


    static public double activationFunction(double x) {
        return 1/(1+Math.exp(-x));
    }
}
