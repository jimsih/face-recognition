import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by albin on 10/5/15.
 */
public class ImageParserTest {
    /*
    *   Test generated by: albin - 10/5/15
    *
    */
    @Test
    public void shouldParseAllImages() {
       ImageParser im = new ImageParser("training.txt", "training-facit.txt");
       assertTrue(im.numberOfImages()==300);
    }

    /*
    *   Test generated by: albin - 10/5/15
    *
    */
    @Test
    public void shouldReturnTheRightImageSizeOf134() {
        ImageParser im = new ImageParser("training.txt", "training-facit.txt");
        assertEquals(im.getImages().get(134).getImg().length, 20);
    }

    /*
    *   Test generated by: albin - 10/5/15
    *
    */
    @Test
    public void shouldFirstElementBeEqualTo0Image1() {
        ImageParser im = new ImageParser("training.txt", "training-facit.txt");

        Double[][] img= im.getImages().get(0).getImg();
        assertTrue(img[0][0] == 0);
    }


    /*
*   Test generated by: albin - 10/5/15
*
*/
    @Test
    public void shouldLastElementBeEqualTo2Image300() {
        ImageParser im = new ImageParser("training.txt", "training-facit.txt");

        Double[][] img= im.getImages().get(299).getImg();
        assertTrue(img[19][19] == 2);
    }

    /*
*   Test generated by: albin - 10/5/15
*
*/
    @Test
    public void shouldMidElementBeEqualTo3Image134() {
        ImageParser im = new ImageParser("training.txt", "training-facit.txt");

        Double[][] img= im.getImages().get(133).getImg();
        assertTrue(img[10][9] == 3);
    }

    /*
    *   Test generated by: albin - 10/5/15
    *
    */
    @Test
    public void shouldHaveFacit() {
        ImageParser im = new ImageParser("training.txt", "training-facit.txt");
        assertTrue(im.getImages().get(134).hasFacit());
    }

    /*
    *   Test generated by: albin - 10/5/15
    *
    */
    @Test
    public void shouldNotHaveToUseAFacitFile() {
        ImageParser im = new ImageParser("training.txt", null);
    }

    /*
    *   Test generated by: albin - 10/5/15
    *
    */
    @Test
    public void shouldNotHaveToGiveAFacitFile() {
        ImageParser im = new ImageParser("training.txt");
    }
}
