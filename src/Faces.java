import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by c12arr on 2015-10-05.
 */
public class Faces {

    public static void main(String[] args) {

        ImageParser imgPTrain = new ImageParser(args[0], args[1]);


        ArrayList<Image> images =  imgPTrain.getImages();
        int nrOfPixels = images.get(0).getImg().length*images.get(0).getImg().length;
        Network netty = new Network(nrOfPixels,4);

        double avgErr = 1;
        int nrOfWrong = 0;
        int nrOfRight = 0;

        double alpha = 0.0001;
       while(avgErr > 0.0002) {
            Collections.shuffle(images, new Random(System.nanoTime()));
            //Trainging

            ArrayList<Image> trainingList = new ArrayList<Image>(images.subList(0, (int) (imgPTrain.numberOfImages() * 0.7)));
            ArrayList<Image> testList = new ArrayList<Image>(images.subList((int) (imgPTrain.numberOfImages() * 0.7) + 1, imgPTrain.numberOfImages()));

            for(Image img : trainingList) {

                netty.train(img, alpha);
            }
            for(Image img : testList) {
                State guess = netty.test(img);

                if(guess == img.getFacit()) {
                    nrOfRight++;
                }
                else {
                    nrOfWrong++;
                }
            }
            avgErr = nrOfWrong/(double)(nrOfRight+nrOfWrong);

           //System.out.println(avgErr);

       }

        ImageParser imgPTest = new ImageParser(args[2]);

        int index = 1;
        for(Image img : imgPTest.getImages()) {
            State guess = netty.test(img);

            System.out.println("Image" + index + " " + State.parseState(guess));
            index++;
        }
    }

}
