/**
 * Created by tfy12jsg on 2015-10-05.
 */
public enum State {
    HAPPY, SAD, MISCHIEVOUS, MAD;

    static public State parseState(int i) {
        switch (i) {
            case 1:
                return State.HAPPY;
            case 2:
                return State.SAD;
            case 3:
                return State.MAD;
            case 4:
                return State.MISCHIEVOUS;
            default:
                return null;
        }
    }

    static public int parseState(State state) {
        if (state == State.HAPPY) {
            return 1;
        } else if (state == State.SAD) {
            return 2;
        } else if (state == State.MAD) {
            return 3;
        } else if (state == State.MISCHIEVOUS) {
            return 4;
        } else {
            return 0;
        }
    }
}
