/**
 * Created by tfy12jsg on 2015-10-05.
 */
public class Image {

    public Double[][] img;
    private State facit;

    public Image(Double[][] img) {
        this.img = img;
        facit = null;
    }

    public Image(Double[][] img, State facit) {
        this.img = img;
        this.facit = facit;
    }

    public void setFacit(State facit) {
        this.facit = facit;
    }

    public boolean hasFacit() {
        return facit == null ? false:true;
    }

    public State getFacit() {
        return facit;
    }

    public Double[][] getImg() {
        return img;
    }
}
