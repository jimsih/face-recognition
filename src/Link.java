/**
 * Created by tfy12jsg on 2015-10-05.
 */
public class Link {

    private int node;
    private double weight;

    public Link(int node, double weight) {
        this.node = node;
        this.weight = weight;
    }

    public int getInputNode() {
        return node;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
