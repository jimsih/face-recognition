import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by c12arr on 2015-10-05.
 * A class to parse a image file
 */
public class ImageParser {

    private BufferedReader imageReader;
    private BufferedReader facitReader;
    private ArrayList<Image> images;
    private boolean facitAvilable;


    /**
     * Parsing the image file and a facit file.
     * @param imageFileName -
     * @param facitFileName
     */
    public ImageParser(String imageFileName, String facitFileName) {
        try {
            imageReader = new BufferedReader(new FileReader(imageFileName));
            if(facitFileName != null) {
                facitReader = new BufferedReader(new FileReader(facitFileName));
                facitAvilable = true;
            } else {
                facitAvilable = false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        images = new ArrayList<Image>();
        try {
            this.parse();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     *  Parses a image file, setting the facit value to null.
     * @param imageFileName
     */
    public ImageParser(String imageFileName) {
        try {
            imageReader = new BufferedReader(new FileReader(imageFileName));
            facitAvilable = false;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        images = new ArrayList<Image>();
        try {
            this.parse();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
       Parse image for image and fill the list.
     */
    private void parse() throws IOException {
        Double[][] tmpImg;
        State tmpState;

        //Read in a image and corresponding facit.
        tmpImg = readImage();
        tmpState = readFacit();

        //For every image file..
        while (tmpImg != null) {
             //Add the image
            Image imgy = new Image(tmpImg, tmpState);

            images.add(imgy);

            //read in a new image.
            tmpImg = readImage();
            tmpState = readFacit();
        }
    }

    /*
        Parses a image. Image must be square
     */
    private Double[][] readImage() throws IOException {

        Double [][] image;
        Integer size;

        //Check for EOF
        String tmpLine = imageReader.readLine();
        if(tmpLine == null) {
            return null;
        }

        //Read until new image.
        while(!tmpLine.startsWith("Image")){
            tmpLine =imageReader.readLine();
            if(tmpLine == null) {
                return null;
            }
        }

        //Read in the image
        tmpLine =imageReader.readLine();
        size = tmpLine.split(" ").length;
        image = new Double[size][size];
        for(int i = 0 ; i < size ; i++) {
           for(int j = 0 ; j < tmpLine.split(" ").length ; j++) {
               image[i][j] = Double.parseDouble((tmpLine.split(" ")[j]));
           }
            tmpLine = imageReader.readLine();
        }

        return image;
    }


    private State readFacit() throws IOException {
        //If there isn't a facit return null
        if (!facitAvilable) {
            return null;
        }

        //Read in the next line
        String tmpLine = facitReader.readLine();

        //If it is the end of the file return null;
        if(tmpLine == null) {
            facitAvilable = false;
            return null;
        }
        //Read a new line unitl the next valid line
        while(!(tmpLine.startsWith("Image")) && tmpLine.split(" ").length != 2) {
            tmpLine = facitReader.readLine();
            if(tmpLine == null) {
                facitAvilable = false;
                return null;
            }
        }

        //Return the value
        return  State.parseState(Integer.parseInt((tmpLine.split(" "))[1]));

    }

    public Integer numberOfImages() {
        return images.size();
    }
    public ArrayList<Image> getImages() {
        return images;
    }
}
