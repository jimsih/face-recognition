import java.util.Arrays;

/**
 * Created by albin on 10/10/15.
 */
public class ImageProcesser {

    private Position posConv;
    private Double[][] cord;
    private int size;
    private Image img;

    public ImageProcesser(Image img) {
        this.img = img;
        size = img.getImg()[0].length;
        posConv = new Position(size,size);
    }


    public double aproximateRotation() {
        double angle = 0;

        for(int y = 0 ; y < size  ; y++) {
            for(int x = 0; x < size ; x++) {
                if(img.getImg()[y][x] >= 30) {
                    angle = Math.atan2(posConv.getDoublePosition(x,y)[1], posConv.getDoublePosition(x,y)[0]);
                    //double bearing = 3*Math.PI/4 -angle;
                    double bearing = angle;
                    System.out.println(angle);
                    if(bearing >= 7*Math.PI/4 && bearing < Math.PI/4) {
                        return Math.PI/2;
                    } else if(bearing >= 3*Math.PI/4 && bearing < 5*Math.PI/4) {
                        return Math.PI;
                    } else if(bearing >= 5*Math.PI/4 && bearing < 7*Math.PI/4) {
                        return 3*Math.PI/2;
                    }
                    return 0;
                }
            }
        }
        return 0;
    }

    public Image rotate(double angle) {

        if (angle == 0)
            return img;

        int[] newPos;
        double[] newDPos;

        Double[][] output = new Double[size][size];

        for(int i = 0 ; i < size ; i++) {
            for (int j = 0 ; j < size ; j++) {
                output[i][j] = 0.0;
            }
        }


        for(int y = 0 ; y  < size ; y++) {
            for(int x = 0 ; x < size ; x++) {

                newDPos =posConv.getDoublePosition(y, x);
                double [] oldDPos = new double[2];

                oldDPos[0] = newDPos[0];
                oldDPos[1] = newDPos[1];

                newDPos[0] = oldDPos[0]*Math.cos(angle) - oldDPos[1]*Math.sin(angle);
                newDPos[1] = oldDPos[0]*Math.sin(angle) + oldDPos[1]*Math.cos(angle);
                newPos  = posConv.getPixelPosition(newDPos);

              /*  System.out.println("OldPixelX: "+x+" OldPixelY: "+y);
                System.out.println("OldDposX: " +oldDPos[0] + " OldDposY: "+ oldDPos[1]);
                System.out.println("NewDPosX: " +newDPos[0] + " NewDPosY: "+ newDPos[1]);
                System.out.println("NewPixelX: " + newPos[0] + " NewPixelY: " + newPos[1]);
                */

                if(newPos[0] >= size || newPos[1] >= size || newPos[0] < 0 || newPos[1] < 0) {
                    continue;
                }

                output[newPos[1]][newPos[0]] = img.getImg()[y][x];
            }
        }
        return(new Image(output, img.getFacit()));
    }

    private class Position {

        private int xSize;
        private int ySize;

        public Position(int xSize, int ySize) {
            this.xSize = xSize;
            this.ySize = ySize;
        }

        public double[] getDoublePosition(int x, int y) {
            double [] output = new double[2];
            output[0] = (double)x-xSize/2;
            output[1] = (double)ySize/2-y;
            return output;
        }

        public int[] getPixelPosition(double [] pos) {
            int[] output = new int[2];
            output[0] = xSize/2+(int)(Math.round(pos[0])-1);
            output[1] = ySize/2- (int)(Math.round(pos[1])+1);
            return output;
        }
    }
}
