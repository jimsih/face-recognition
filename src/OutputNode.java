import java.util.ArrayList;

/**
 * Created by tfy12jsg on 2015-10-05.
 */
public class OutputNode {

    private ArrayList<Link> links;
    private double value;

    public OutputNode(ArrayList<Link> links) {
        this.links = links;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }


}
